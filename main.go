package main

import (
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/widget"
)

func main() {
	myApp := app.NewWithID("a")
	myWin := myApp.NewWindow("a")
	meta := myApp.Metadata()
	but1 := widget.NewButton("StrFlw", nil)
	but2 := widget.NewButton("StrFlw", nil)
	tabs := container.NewAppTabs(
		container.NewTabItem(meta.Version, container.NewVBox(but1)),
		container.NewTabItem("2", container.NewVBox(but2)),
		//container.NewTabItem("3", makeControlsCfg(myWin)),
	)
	myWin.ShowAndRun(tabs)
	//myApp.Run()
}
